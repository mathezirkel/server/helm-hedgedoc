# HedgeDoc Helm chart

![Version: 4.0.1](https://img.shields.io/badge/Version-4.0.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.10.0-alpine](https://img.shields.io/badge/AppVersion-1.10.0--alpine-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [HedgeDoc](https://hedgedoc.org/) on Kubernetes.
HedgeDoc is a collaborative markdown editor that allows multiple users to edit a document simultaneously in real-time.

We use the official docker image of [HedgeDoc](https://github.com/hedgedoc/container).

The chart requires an existing [PostgreSQL](https://www.postgresql.org/) database,
[Traefik v2](https://traefik.io/traefik/) with its associated CRDs,
and [Argo Workflows](https://argoproj.github.io/workflows/) along with its CRDs if backups are enabled.

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace hedgedoc
    ```

2. Create a secret for the hedgedoc database crendentials.

    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=database=hedgedoc \
    --from-literal=user=hedgedocuser \
    --from-literal=password=CHANGEME \
    -n hedgedoc
    ```

3. Create a database user and a database in the internal database using the [script](./database-setup/database-setup.sql). Don't forget to modify the password.

4. Create a secret for session signing.

    ```shell
    kubectl create secret generic session-secret \
    --from-literal=secret=CHANGEME \
    -n hedgedoc
    ```

5. Adjust the configuration in [values.yaml](./values.yaml).

6. Deploy the helm chart

    ```shell
    helm upgrade --install -n hedgedoc hedgedoc .
    ```

## Connecting Keycloak

1. Keycloak configuration
    1. Create new client
    2. Configure client
    3. Copy client secret
        Clients -> hedgedoc-auth -> Credentials -> Client secret

2. Create Kubernetes secret

    ```shell
    kubectl create secret generic hedgedoc-client-secret \
        --from-literal=secret=CHANGEME \
        -n hedgedoc
    ```

3. Hedge Configuration at `.Values.config.oauth2`. For details follow [this guide](https://docs.hedgedoc.org/guides/auth/keycloak/).

## Set up backup

The backup depends on a existing [restic](https://restic.readthedocs.io) repository available via sftp.

1. Initialize a restic repository.

    ```shell
    restic -r sftp:<ssh-alias>:<path-to-repository> init
    ```

2. Create a secret for the repository crendentials.

    ```shell
    kubectl create secret generic backup-restic-credentials \
    --from-literal=password=CHANGEME \
    -n hedgedoc
    ```

3. Create a secret for the SSH Key.

    ```shell
    ssh-keyscan -t rsa,ecdsa,ed25519 -H -p CHANGEME CHANGEME > /tmp/known_hosts
    ```

    ```shell
    kubectl create secret generic backup-ssh-credentials \
    --from-literal=hostname=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=path=CHANGEME \
    --from-file=knownHosts=/tmp/known_hosts \
    --from-file=privateKey=CHANGEME \
    -n hedgedoc
    ```

4. Create a secret for mail credentials.
    For comprehensive documentation, refer to [python-sendmail](https://gitlab.com/mathezirkel/server/python-sendmail).

    ```shell
    kubectl create secret generic backup-mail-credentials \
    --from-literal=host=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=encryptionMode=CHANGEME \
    --from-literal=username=CHANGEME \
    --from-literal=password=CHANGEME \
    --from-literal=from=CHANGEME \
    --from-literal=to=CHANGEME \
    -n hedgedoc
    ```

5. Test your backup regularly! Detailed instructions can be found [here](https://gitlab.com/mathezirkel/server/helm-hedgedoc/-/tree/main/backup/).

## HedgeDoc Documentation

For a complete documentation see [https://docs.hedgedoc.org](https://docs.hedgedoc.org).

### High availability and scaling

Unfortunately, it is not possible to run multiple instances of HedgeDoc on the same database, see [https://docs.hedgedoc.org/faq/#can-i-run-multiple-instances-on-the-same-database]

## Values

### Backup

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| backup.cleanUpPolicy.keepDaily | int | `7` | Number of recent daily snapshots to keep |
| backup.cleanUpPolicy.keepLast | int | `3` | Number of recent snapshots to keep |
| backup.cleanUpPolicy.keepMonthly | int | `6` | Number of recent monthly snapshots to keep |
| backup.cleanUpPolicy.keepWeekly | int | `4` | Number of recent weekly snapshots to keep |
| backup.databaseDumpTempVolumeSize | string | `"1Gi"` | Size of the temporarily generated volume saving the database dumps |
| backup.enabled | bool | `true` | Enable the backup. |
| backup.failedJobsHistoryLimit | int | `2` | Number of failed jobs kept in history |
| backup.lastSuccessfulBackupThreshold | int | `24` | Maximum allowable age in hours for the most recent successful backup |
| backup.notification.existingSecret.encryptionModeKey | string | `"encryptionMode"` | Key containing the encryption method (Possible values: `ssl` or `starttls`) |
| backup.notification.existingSecret.fromKey | string | `"from"` | Key containing the email address sending the notification |
| backup.notification.existingSecret.hostKey | string | `"host"` | Key containing the hostname of the SMTP server |
| backup.notification.existingSecret.name | string | `"backup-mail-credentials"` | Name of an existing secret containing mail credentials for notification |
| backup.notification.existingSecret.passwordKey | string | `"password"` | Key containing the password for authentication |
| backup.notification.existingSecret.portKey | string | `"port"` | Key containing the SMTP port |
| backup.notification.existingSecret.toKey | string | `"to"` | Key containing an email address to receive backup notification |
| backup.notification.existingSecret.usernameKey | string | `"username"` | Key containing username for authentication |
| backup.pullPolicy | string | `"IfNotPresent"` | Workflow image pull policy |
| backup.restic.credentials.repository | object | `{"key":"password","name":"backup-restic-credentials"}` | Existing secret containing the repository credentials |
| backup.restic.credentials.repository.key | string | `"password"` | Key containing the password |
| backup.restic.credentials.repository.name | string | `"backup-restic-credentials"` | Name of the secret |
| backup.restic.credentials.ssh | object | `{"hostnameKey":"hostname","knownHostsKey":"knownHosts","name":"backup-ssh-credentials","pathKey":"path","portKey":"port","privateKeyKey":"privateKey","userKey":"user"}` | Existing secret containing the SSH credentials of the backup server |
| backup.restic.credentials.ssh.hostnameKey | string | `"hostname"` | Key containing the hostname |
| backup.restic.credentials.ssh.knownHostsKey | string | `"knownHosts"` | Key containing the known_hosts file |
| backup.restic.credentials.ssh.name | string | `"backup-ssh-credentials"` | Name of the secret |
| backup.restic.credentials.ssh.pathKey | string | `"path"` | Key containing the path |
| backup.restic.credentials.ssh.portKey | string | `"port"` | Key containing the port |
| backup.restic.credentials.ssh.privateKeyKey | string | `"privateKey"` | Key containing the private SSH key |
| backup.restic.credentials.ssh.userKey | string | `"user"` | Key containing the user |
| backup.restic.image.repository | string | `"restic/restic"` | Restic image repository |
| backup.restic.image.tag | string | `"0.17.0"` | Restic image tag Same as version on backup server |
| backup.schedule.backup | string | `"0 2 * * *"` | Schedule of the backup |
| backup.schedule.check | string | `"0 10 * * *"` | Schedule of the backup check |
| backup.schedule.clean | string | `"0 18 * * 0"` | Schedule of the backup clean up |
| backup.successfulJobsHistoryLimit | int | `1` | Number of successful jobs kept in history |

### Config

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.allowAnonymous | bool | `false` | Set to allow anonymous usage. |
| config.allowAnonymousEdits | bool | `false` | If allowAnonymous is false: allow users to select freely permission, allowing guests to edit existing notes. |
| config.allowEmailRegister | bool | `false` | Set to allow registration of new accounts using an email address. See documentation for details. |
| config.allowFreeURL | bool | `true` | Set to allow new note creation by accessing a nonexistent note URL. This is the behavior familiar from [Etherpad](https://github.com/ether/etherpad-lite). |
| config.allowGravatar | bool | `false` | Set to false to disable Libravatar as profile picture source on your instance. Libravatar is a federated open-source alternative to Gravatar. |
| config.allowOrigin | list | `["localhost","pad.mathezirkel-augsburg.de"]` | Domain name allowlist |
| config.cookiePolicy | string | `"lax"` | Set a SameSite policy whether cookies are send from cross-origin. See documentation for details. |
| config.csp.addDefaults | bool | `true` | Disable to not include the default CSP. |
| config.csp.addDisqus | bool | `false` | Enable to allow users to add Disqus comments to their notes or presentations. |
| config.csp.addGoogleAnalytics | bool | `false` | Enable to allow users to add Google Analytics to their notes. |
| config.csp.allowFraming | bool | `false` | Disable to disallow embedding of the instance via iframe. |
| config.csp.allowPDFEmbed | bool | `false` | Disable to disallow embedding PDFs. |
| config.csp.directives | object | `{}` | Custom CSP directives. These are passed to Helmet - see their documentation for more information on the format. |
| config.csp.enable | bool | `true` | Whether to apply a Content-Security-Policy header to responses. |
| config.csp.upgradeInsecureRequests | string | `"auto"` | By default (auto), insecure (HTTP) requests are upgraded to HTTPS via CSP if useSSL is on. To change this behaviour, set to either true or false. |
| config.debug | bool | `false` | Enable debug mode |
| config.defaultPermission | string | `"limited"` | Set notes default permission (only applied on signed-in users). Possible values: editable, freely, limited, locked, protected, private |
| config.documentMaxLength | int | `100000` | Note max length |
| config.domain | string | `"pad.mathezirkel-augsburg.de"` | Domain name |
| config.email | bool | `false` | Set to allow email sign-in. |
| config.forbiddenNoteIDs | list | `["robots.txt","favicon.ico","api","build","css","docs","fonts","js","uploads","vendor","views"]` | Disallow creation of notes with names contained in this list, even if allowFreeUrl is true. |
| config.heartbeatInterval | int | `5000` | socket.io heartbeat interval |
| config.heartbeatTimeout | int | `10000` | socket.io heartbeat timeout |
| config.hsts.enable | bool | `false` | Set to enable HSTS if HTTPS is also enabled. |
| config.hsts.includeSubdomains | bool | `true` | Set to include subdomains in HSTS. |
| config.hsts.maxAgeSeconds | int | 60 * 60 * 24 * 365 | Max duration in seconds to tell clients to keep HSTS status |
| config.hsts.preload | bool | `false` | Whether to allow preloading of the site's HSTS status (e.g. into browsers). |
| config.linkifyHeaderStyle | string | `"lower-case"` | How is a header text converted into a link Possible values: keep-case, lower-case, gfm |
| config.loglevel | string | `"info"` | Loglevel. Possible values: debug, verbose, info, warn, error |
| config.oauth2.authorizationURL | string | `"https://auth.mathezirkel-augsburg.de/realms/mathezirkel/protocol/openid-connect/auth"` | Authorization URL of your provider, please refer to the documentation of your OAuth2 provider. |
| config.oauth2.clientID | string | `"hedgedoc-auth"` | You will get this from your OAuth2 provider when you register HedgeDoc as OAuth2-client. |
| config.oauth2.clientSecret | object | `{"existingSecret":{"key":"secret","name":"hedgedoc-client-secret"}}` | You will get this from your OAuth2 provider when you register HedgeDoc as OAuth2-client. |
| config.oauth2.clientSecret.existingSecret.key | string | `"secret"` | Key containing the secret |
| config.oauth2.clientSecret.existingSecret.name | string | `"hedgedoc-client-secret"` | Name of an existing secret containing the client secret. |
| config.oauth2.enabled | bool | `true` | Enable oauth2 login |
| config.oauth2.scope | string | `"openid email profile"` | Scope to request for OIDC (OpenID Connect) providers. |
| config.oauth2.tokenURL | string | `"https://auth.mathezirkel-augsburg.de/realms/mathezirkel/protocol/openid-connect/token"` | Sometimes called token endpoint, please refer to the documentation of your OAuth2 provider. |
| config.oauth2.userProfileDisplayNameAttr | string | `"name"` | Where to find the display-name in the JSON from the user profile URL. |
| config.oauth2.userProfileEmailAttr | string | `"email"` | Where to find the display-name in the JSON from the user profile URL. |
| config.oauth2.userProfileURL | string | `"https://auth.mathezirkel-augsburg.de/realms/mathezirkel/protocol/openid-connect/userinfo"` | Where to retrieve information about a user after successful login. |
| config.oauth2.userProfileUsernameAttr | string | `"preferred_username"` | Where to find the username in the JSON from the user profile URL. |
| config.protocolUseSSL | bool | `true` | Set to use SSL protocol for resources path (only applied when domain is set) |
| config.requireFreeURLAuthentication | bool | `true` | Set to require authentication for FreeURL mode style note creation. |
| config.sessionLife | int | 14 * 24 * 60 * 60 * 1000 | Cookie session life time in milliseconds. |
| config.staticCacheTime | int | 1 * 24 * 60 * 60 * 1000 | Static file cache time |
| config.tooBusyLag | int | `70` | CPU time for one event loop tick until node throttles connections. (milliseconds) |
| config.urlAddPort | bool | `false` | Set to add port on callback URL (ports 80 or 443 won't be applied) (only applied when domain is set) |
| config.useSSL | bool | `false` | Set to use SSL server (if true, will auto turn on protocolUseSSL) |
| session.existingSecret | object | `{"key":"secret","name":"session-secret"}` | Cookie session secret used to sign the session cookie. |
| session.existingSecret.key | string | `"secret"` | Key containing the cookie session secret |
| session.existingSecret.name | string | `"session-secret"` | Name of the existing secret |

### Database

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| database.existingSecret.databaseKey | string | `"database"` | Key containing the database name |
| database.existingSecret.name | string | `"database-credentials"` | Name of an existing secret containing the database credentials |
| database.existingSecret.passwordKey | string | `"password"` | Key containing the user password |
| database.existingSecret.userKey | string | `"user"` | Key containing the database user |
| database.host | string | `"postgresql17.database.svc.cluster.local"` | External database service |
| database.port | int | `5432` | Port of the external database |
| database.vendor | string | `"postgres"` | Database vendor Possible values: postgres, mariadb |
| database.version | string | `"17.2-alpine"` | Database version |

### Deployment

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` | String to fully override nextcloud.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | HedgeDoc image pull policy |
| image.repository | string | `"quay.io/hedgedoc/hedgedoc"` | HedgeDoc image repository |
| image.tag | string | Chart.appVersion | HedgeDoc image tag |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on HedgeDoc Primary containers |
| livenessProbe.failureThreshold | int | `5` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `5` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `6` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | String to partially override nextcloud.fullname |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on HedgeDoc containers |
| readinessProbe.failureThreshold | int | `5` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `5` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `2` | Timeout seconds for readinessProbe |
| resources.limits.memory | string | `"1024Mi"` | The memory limits for the HedgeDoc containers |
| resources.requests.cpu | string | `"100m"` | The requested cpu for the HedgeDoc containers |
| resources.requests.memory | string | `"256Mi"` | The requested memory for the HedgeDoc containers |
| service.port | int | `3000` | HedgeDoc service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for HedgeDoc pod. If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | hedgedoc.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on HedgeDoc containers |
| startupProbe.failureThreshold | int | `5` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `20` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `5` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| terminationGracePeriodSeconds | string | `""` | Seconds Hedgedoc pod needs to terminate gracefully |

### Ingress

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.additionalMiddlewares | list | `[{"name":"security-headers","namespace":"kube-public"}]` | List of additional middlewares to apply on the ingress |
| ingress.enabled | bool | `true` | Enable the ingress |
| ingress.entryPoints | list | `["websecure"]` | List of traefik entrypoints to listen |
| ingress.forbiddenPaths | list | `["/metrics","/status"]` | Paths not allowed to access from external |
| ingress.host | string | `"pad.mathezirkel-augsburg.de"` | Hostname of the HedgeDoc instance |
| ingress.tls.certResolver | string | `"lets-encrypt"` | Name of the certResolver configured via traefik |

### Persistence

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| persistence.accessMode | string | `"ReadWriteOnce"` | PVC Access Mode for HedgeDoc uploads volume |
| persistence.size | string | `"20Gi"` | PVC Storage Request for HedgeDoc uploads volume |
| persistence.storageClassName | string | `"retain-local-path"` | PVC StorageClass for HedgeDoc uploads volume |

## License

This project is licenced under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](./LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
