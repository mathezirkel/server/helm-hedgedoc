{{- if .Values.backup.enabled }}
apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: {{ printf "%s-backup" (include "hedgedoc.fullname" .) }}
  generateName: {{ printf "%s-backup-" (include "hedgedoc.fullname" .) }}
  namespace: {{ .Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ printf "%s-backup" (include "hedgedoc.fullname" .) }}
    app.kubernetes.io/component: backup
    {{- include "hedgedoc.labels" . | nindent 4 }}
spec:
  workflowMetadata:
    labels:
      app.kubernetes.io/name: {{ printf "%s-backup" (include "hedgedoc.fullname" .) }}
      app.kubernetes.io/component: backup
      {{- include "hedgedoc.labels" . | nindent 4 }}
  serviceAccountName: {{ printf "%s-backup" (include "hedgedoc.serviceAccountName" .) }}
  entrypoint: backup
  ttlStrategy:
    secondsAfterSuccess: 600
  onExit: exit-handler
  volumes:
    - name: hedgedoc-uploads
      persistentVolumeClaim:
        claimName: {{ include "hedgedoc.fullname" . }}
    - name: backup-ssh-credentials
      secret:
        secretName: {{ .Values.backup.restic.credentials.ssh.name }}
        defaultMode: 256
  volumeClaimTemplates:
    - metadata:
        name: {{ printf "%s-backup-database" (include "hedgedoc.fullname" .) }}
        namespace: {{ .Release.Namespace }}
        labels:
          app.kubernetes.io/name: {{ printf "%s-backup-database" (include "hedgedoc.fullname" .) }}
          app.kubernetes.io/component: backup
          {{- include "hedgedoc.labels" . | nindent 10 }}
      spec:
        resources:
          requests:
            storage: {{ .Values.backup.databaseDumpTempVolumeSize }}
        volumeMode: Filesystem
        storageClassName: local-path
        accessModes:
          - ReadWriteOnce
  volumeClaimGC:
    strategy: OnWorkflowCompletion
  templates:
    - name: backup
      steps:
        - - name: pg-dump
            template: pg-dump
          - name: pg-sql
            template: pg-sql
        - - name: restic
            template: restic
    - name: pg-dump
      script:
        image: {{ printf "postgres:%s" .Values.database.version }}
        imagePullPolicy: {{ .Values.backup.pullPolicy }}
        command: [/bin/sh]
        source: |
          pg_dump \
          -v \
          -Fc \
          -b \
          -f "/backup/{{ include "hedgedoc.fullname" . }}.dump"
        env:
          - name: PGHOST
            value: {{ .Values.database.host }}
          - name: PGPORT
            value: {{ .Values.database.port | quote }}
          - name: PGDATABASE
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.databaseKey }}
          - name: PGUSER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.userKey }}
          - name: PGPASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.passwordKey }}
        volumeMounts:
          - name: {{ printf "%s-backup-database" (include "hedgedoc.fullname" .) }}
            mountPath: /backup/
    - name: pg-sql
      script:
        image: {{ printf "postgres:%s" .Values.database.version }}
        imagePullPolicy: {{ .Values.backup.pullPolicy }}
        command: [/bin/sh]
        source: |
          pg_dump \
          -v \
          -f "/backup/{{ include "hedgedoc.fullname" . }}.sql"
        env:
          - name: PGHOST
            value: {{ .Values.database.host }}
          - name: PGPORT
            value: {{ .Values.database.port | quote }}
          - name: PGDATABASE
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.databaseKey }}
          - name: PGUSER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.userKey }}
          - name: PGPASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Values.database.existingSecret.name }}
                key: {{ .Values.database.existingSecret.passwordKey }}
        volumeMounts:
          - name: {{ printf "%s-backup-database" (include "hedgedoc.fullname" .) }}
            mountPath: /backup/
    - name: restic
      script:
        image: "{{ .Values.backup.restic.image.repository }}:{{ .Values.backup.restic.image.tag }}"
        imagePullPolicy: {{ .Values.backup.pullPolicy }}
        command: [/bin/sh]
        source: |
          restic \
          -r \
          "sftp:${SSH_USER}@${SSH_HOSTNAME}:${REPOSITORY_PATH}" \
          -o sftp.args="-p ${SSH_PORT} -i /root/.ssh/id -o ServerAliveInterval=60 -o ServerAliveCountMax=240" \
          --group-by paths \
          --verbose=2 \
          backup \
          /backup
        env:
          - name: SSH_HOSTNAME
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.hostnameKey }}
          - name: SSH_PORT
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.portKey }}
          - name: SSH_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.userKey }}
          - name: REPOSITORY_PATH
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.ssh.name }}
                key: {{ .Values.backup.restic.credentials.ssh.pathKey }}
          - name: RESTIC_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Values.backup.restic.credentials.repository.name }}
                key: {{ .Values.backup.restic.credentials.repository.key }}
        volumeMounts:
          - name: {{ printf "%s-backup-database" (include "hedgedoc.fullname" .) }}
            mountPath: /backup/database/
            readOnly: true
          - name: hedgedoc-uploads
            mountPath: /backup/uploads/
            readOnly: true
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/known_hosts
            subPath: {{ .Values.backup.restic.credentials.ssh.knownHostsKey }}
            readOnly: true
          - name: backup-ssh-credentials
            mountPath: /root/.ssh/id
            subPath: {{ .Values.backup.restic.credentials.ssh.privateKeyKey }}
            readOnly: true
    - name: exit-handler
      steps:
        - - name: failure-notification
            arguments:
              parameters:
                - name: type
                  value: "Backup"
            templateRef:
              name: {{ printf "%s-backup-exit" (include "hedgedoc.fullname" .) }}
              template: failure-notification
            when: "{{ `{{workflow.status}}` }} != Succeeded"
{{- end }}
