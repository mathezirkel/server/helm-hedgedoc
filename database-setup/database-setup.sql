-- Add user for postgresql
CREATE USER hedgedocuser WITH PASSWORD 'CHANGEME';

-- Add database for nextcloud
CREATE DATABASE hedgedoc WITH OWNER=hedgedocuser;
