# Backups

## Manually create a backup

### Argo CLI

```shell
argo submit --from workflowtemplate/hedgedoc-backup -n hedgedoc
```

## Test a backup

1. Pull a snapshot from the restic repository.

    ```shell
    restic -r CHANGEME restore latest --target ./backup_io/
    ```

2. Start the dev setup

    ```shell
    docker compose up
    ```

    The dev setup automatically restores the `.sql` file from the backup. If you also want to test the `.dump` file, execute

    ```shell
    docker compose run --rm database pg_restore -U hedgedocuser --clean -d databasename /backup_io/hedgedoc.dump
    ```

3. The Hedgedoc instance is available under [localhost:3000](localhost:3000).
Just register, login and check, if the notes are available.
